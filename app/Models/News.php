<?php
namespace App\Models;

use Illuminate\Support\Facades\Storage;

class News
{
    private array $news = [
      [
          'id' => 1,
          'title' => 'Новость спорта 1',
          'text' => 'Здесь первая новость спорта',
          'category' => 'sport',
          'isPrivate' => true
      ],
      [
          'id' => 2,
          'title' => 'Новость спорта 2',
          'text' => 'здесь вторая новость спорта',
          'category' => 'sport',
          'isPrivate' => false
      ],
      [
          'id' => 3,
          'title' => 'Новость погоды 1',
          'text' => 'здесь первая новость погоды',
          'category' => 'pogoda',
          'isPrivate' => false
      ],
      [
          'id' => 4,
          'title' => 'Новость погоды 2',
          'text' => 'здесь вторая новость погоды',
          'category' => 'pogoda',
          'isPrivate' => true
      ],
    ];

    public function getNews(){
//            Storage::disk('local')->put('news.json', json_encode($this->news, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
            return json_decode(Storage::disk('local')->get('news.json'), true);
    }

    public function getNewsId($id){
        foreach ($this->getNews() as $news){
            if ($news['id'] == $id){
                return $news;
            }
        }
       return null;
    }
}
