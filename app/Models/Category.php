<?php
namespace App\Models;

use Illuminate\Support\Facades\Storage;

class Category
{
    private array $categories = [
        [
            'id' => '1',
            'title' => 'Новости спорта',
            'slug' => 'sport'

        ],
        [
            'id' => '2',
            'title' => 'Новости погоды',
            'slug' => 'pogoda'
        ]
    ];

    public function getCategories(){
        Storage::disk('local')->put('category.json', json_encode($this->categories, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
        return json_decode(Storage::disk('local')->get('category.json'), true);
    }

    public function getCategoriesId($slug){
        $news = new News();
        $array = [];
        foreach ($news->getNews() as $newsOne){
            if ($newsOne['category'] == $slug){
                $array[] = $newsOne;
            }
        }
        return $array;

    }
}

