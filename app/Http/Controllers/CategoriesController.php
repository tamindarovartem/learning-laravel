<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index(Category $category)
    {
        return view('categories.index')->with('categories', $category->getCategories());
    }

    public function show($slug, Category $category)
    {
        if ($category->getCategoriesId($slug) != null){
            return view('categories.one')->with('categories', $category->getCategoriesId($slug));
        }else{
            return view('404');
        }
    }
}
