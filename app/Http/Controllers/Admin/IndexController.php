<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class IndexController extends Controller
{
    public function index()
    {

        return view('admin.index');
    }

    public function addNews(Request $request, Category $category)
    {
        if($request->isMethod('post')){
            $arrayNews = json_decode(Storage::disk('local')->get('news.json'), true);
            $arrayNews[] = [
                'id' => count($arrayNews) + 1,
                'title' => $request->input('newsName'),
                'text' => $request->input('textNews'),
                'category' => $request->input('category'),
                'isPrivate' => (bool)$request->input('isPrivate')
            ];
            Storage::disk('local')->put('news.json', json_encode($arrayNews, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
//            $request->flash();
            return redirect()->route('newsOne', count($arrayNews));

        }
        return view('admin.addNews', [
            'categories' => $category->getCategories()
        ]);
    }

    public function downloadImg(){
        return response()->download('1.jpg');
    }
    public function downloadText(Request $request, Category $category){
        $arrayNews = json_decode(Storage::disk('local')->get('news.json'), true);
        $categoryList = [];
        if($request->isMethod('post')){
            foreach ($arrayNews as $news){
                if ($request->input('category') === $news['category']){
                    $categoryList[] = $news;
                }elseif ($request->input('category') === 'all'){
                    $categoryList = $arrayNews;
                }
            }
            return response()->json($categoryList)
                ->header('content-Disposition', 'attachment; filename = "json.txt"')
                ->setEncodingOptions(JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        }
        return view('admin.donwnloadText', [
            'categories' => $category->getCategories()
        ]);
    }
}
