@extends('layouts.app')

@section('title')
    @parent Новостей
@endsection

@section('menu')
    @include ("menu")
@endsection

@section('content')
    <h1>Страница новостей</h1>
    @forelse($news as $item)
        <a href="{{ route('newsOne', $item['id']) }}">{{ $item['title'] }}</a><br/>
    @empty
        <p>Новостей нет</p>
    @endforelse
@endsection
