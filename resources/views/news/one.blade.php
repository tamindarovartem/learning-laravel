@extends('layouts.app')

@section('title')
    @parent Новости {{ $news['title'] }}
@endsection

@section('menu')
    @include ("menu")
@endsection

@section('content')
    @if ( !$news['isPrivate'] )
        <h1>{{ $news['title'] }}</h1>
        <p>{{ $news['text'] }}</p>
    @else
        <p>Зарегестрируйтесь для просмотра</p>
    @endif
@endsection
