@extends('layouts.app')

@section('title')
    @parent добавить новость
@endsection

@section('menu')
    @include ("admin.menu")
@endsection

@section('content')
    <h1>Скачать Категорию</h1>

    <form method="POST" action="{{ route('admin.downloadText') }}">
        @csrf
        <label>категория</label><br/>
        <select class="form-control"  name="category">
            @forelse($categories as $category)$categories as $category)
            <option value="{{ $category['slug'] }}">{{ $category['title'] }}</option>
            @empty
                <option value="0">Нет категорий</option>
            @endforelse
                <option value="all">Скачать Все</option>
        </select><br/>

        <input type="submit"  class="btn btn-primary" value="Скачать">
    </form>





@endsection
