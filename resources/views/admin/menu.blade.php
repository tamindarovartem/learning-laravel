
    <li class="nav-item"><a class="nav-link" href="{{ route('home') }}">Главная новости</a></li>
    <li class="nav-item"><a class="nav-link {{ request()->routeIs('admin.index')?'active': '' }}" href="{{ route('admin.index') }}">Главная админка</a></li>
    <li class="nav-item"><a class="nav-link {{ request()->routeIs('admin.addNews')?'active': '' }}" href="{{ route('admin.addNews') }}">Добавить новость</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('admin.downloadImg') }}">Скачать изображение</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ route('admin.downloadText') }}">Скачать текст</a></li>

