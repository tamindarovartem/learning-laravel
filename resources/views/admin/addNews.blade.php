@extends('layouts.app')

@section('title')
    @parent добавить новость
@endsection

@section('menu')
    @include ("admin.menu")
@endsection

@section('content')
        <h1>Добавить новость</h1>

        <form method="POST" action="{{ route('admin.addNews') }}">
            @csrf
            <label>Название новости</label><br/>
            <input class="form-control" type="text" name="newsName" value="{{ old('newsName') }}"><br/>
            <label>Текст новости</label><br/>
            <textarea class="form-control" name="textNews" cols="30" rows="10" value="{{ old('textNews') }}" ></textarea><br/>
            <label>категория</label><br/>
            <select class="form-control"  name="category">
                @forelse($categories as $category)$categories as $category)
                    <option @if(old('category') === $category['slug']) selected @endif value="{{ $category['slug'] }}">{{ $category['title'] }}</option>
                @empty
                    <option value="0">Нет категорий</option>
                @endforelse

            </select><br/>
            <input @if( old('isPrivate') === '1') checked @endif value="1" type="checkbox" name="isPrivate">
            <label>приватная</label><br/>
            <input type="submit"  class="btn btn-primary" value="Добавить">
        </form>





@endsection
