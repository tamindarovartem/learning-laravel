@extends('layouts.app')

@section('title')
    @parent Главная
@endsection

@section('menu')
    @include ("menu")
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <h2>{{ $name }}</h2>
                </div>
            </div>
        </div>
    </div>
@endsection
