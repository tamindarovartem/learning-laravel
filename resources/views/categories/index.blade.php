@extends('layouts.app')

@section('title')
    @parent категорий
@endsection

@section('menu')
    @include ("menu")
@endsection

@section('content')
    <h1>Страница категорий</h1>
    @foreach($categories as $item)
        <a href="{{ route('categoryOne', $item['slug']) }}">{{ $item['title'] }}</a><br/>
    @endforeach
@endsection
