@extends('layouts.app')

@section('title')
    @parent категорий
@endsection

@section('menu')
    @include ("menu")
@endsection

@section('content')
    @foreach($categories as $category)
        <h2>{{ $category['title'] }}</h2>
        <p>{{ $category['text'] }}</p>
    @endforeach
@endsection
