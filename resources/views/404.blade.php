@extends('layouts.main')

@section('title')
    @parent 404
@endsection

@section('menu')
    @include ("menu")
@endsection

@section('content')
    <h1>404 Такой страницы не существует</h1>
@endsection
