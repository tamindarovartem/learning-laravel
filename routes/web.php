<?php

use App\Http\Controllers\Admin\IndexController as AdminIndexController;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\NewsController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoriesController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MainController::class, 'index'])->name('home');

Route::name('admin')
    ->prefix('admin')
    ->group(function (){
        Route::get('/', [AdminIndexController::class, 'index'])->name('.index');
        Route::get('/add', [AdminIndexController::class, 'addNews'])->name('.addNews');
        Route::post('/add', [AdminIndexController::class, 'addNews'])->name('.addNews');
        Route::get('/downloadImg', [AdminIndexController::class, 'downloadImg'])->name('.downloadImg');
        Route::get('/downloadText', [AdminIndexController::class, 'downloadText'])->name('.downloadText');
        Route::post('/downloadText', [AdminIndexController::class, 'downloadText'])->name('.downloadText');
    });



Route::prefix('news')
    ->group(function () {
        Route::get('/categories', [CategoriesController::class, 'index'])->name('categories');
        Route::get('/categories/{slug}', [CategoriesController::class, 'show'])->name('categoryOne');
        Route::get('/', [NewsController::class, 'index'])->name('news');
        Route::get('/{id}', [NewsController::class, 'show'])->name('newsOne');
});



Route::fallback(function (){
    return view('404');
});
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('hometest');
