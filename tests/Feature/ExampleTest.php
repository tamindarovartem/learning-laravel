<?php

namespace Tests\Feature;

// use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\News;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_the_application_returns_a_successful_response()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
        $view = $this->view('index', ['name' => 'Добро пожаловать']);
        $view->assertSee('Добро пожаловать');
    }

    public function test_the_news_returns_a_successful_response()
    {
        $news = new News();
        $response = $this->get('/news');
        $response->assertStatus(200);
//        $view = $this->view('news.index', ['news' => $news->getNews()]);
//        $view->assertSee($news->getNews());
    }

    public function test_the_categories_returns_a_successful_response()
    {
        $response = $this->get('/categories');
        $response->assertStatus(200);
//        $view = $this->view('categories.index', ['name' => 'Добро пожаловать']);
//        $view->assertSee('Добро пожаловать');
    }

    public function test_the_admin_returns_a_successful_response()
    {
        $response = $this->get('/admin');
        $response->assertStatus(200);
//        $view = $this->view('admin.index', ['name' => 'Добро пожаловать']);
//        $view->assertSee('Добро пожаловать');
    }
}
